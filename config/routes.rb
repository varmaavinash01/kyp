Rails.application.routes.draw do
  resources :politicians
  resources :politicalparties
  resources :constituencies
  resources :states
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

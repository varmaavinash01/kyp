require 'test_helper'

class PoliticalpartiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @politicalparty = politicalparties(:one)
  end

  test "should get index" do
    get politicalparties_url
    assert_response :success
  end

  test "should get new" do
    get new_politicalparty_url
    assert_response :success
  end

  test "should create politicalparty" do
    assert_difference('Politicalparty.count') do
      post politicalparties_url, params: { politicalparty: {  } }
    end

    assert_redirected_to politicalparty_url(Politicalparty.last)
  end

  test "should show politicalparty" do
    get politicalparty_url(@politicalparty)
    assert_response :success
  end

  test "should get edit" do
    get edit_politicalparty_url(@politicalparty)
    assert_response :success
  end

  test "should update politicalparty" do
    patch politicalparty_url(@politicalparty), params: { politicalparty: {  } }
    assert_redirected_to politicalparty_url(@politicalparty)
  end

  test "should destroy politicalparty" do
    assert_difference('Politicalparty.count', -1) do
      delete politicalparty_url(@politicalparty)
    end

    assert_redirected_to politicalparties_url
  end
end

class PoliticalpartiesController < ApplicationController
  before_action :set_politicalparty, only: [:show, :edit, :update, :destroy]

  # GET /politicalparties
  # GET /politicalparties.json
  def index
    @politicalparties = Politicalparty.all
  end

  # GET /politicalparties/1
  # GET /politicalparties/1.json
  def show
  end

  # GET /politicalparties/new
  def new
    @politicalparty = Politicalparty.new
  end

  # GET /politicalparties/1/edit
  def edit
  end

  # POST /politicalparties
  # POST /politicalparties.json
  def create
    @politicalparty = Politicalparty.new(politicalparty_params)

    respond_to do |format|
      if @politicalparty.save
        format.html { redirect_to @politicalparty, notice: 'Politicalparty was successfully created.' }
        format.json { render :show, status: :created, location: @politicalparty }
      else
        format.html { render :new }
        format.json { render json: @politicalparty.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /politicalparties/1
  # PATCH/PUT /politicalparties/1.json
  def update
    respond_to do |format|
      if @politicalparty.update(politicalparty_params)
        format.html { redirect_to @politicalparty, notice: 'Politicalparty was successfully updated.' }
        format.json { render :show, status: :ok, location: @politicalparty }
      else
        format.html { render :edit }
        format.json { render json: @politicalparty.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /politicalparties/1
  # DELETE /politicalparties/1.json
  def destroy
    @politicalparty.destroy
    respond_to do |format|
      format.html { redirect_to politicalparties_url, notice: 'Politicalparty was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_politicalparty
      @politicalparty = Politicalparty.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def politicalparty_params
      params.fetch(:politicalparty, {})
    end
end

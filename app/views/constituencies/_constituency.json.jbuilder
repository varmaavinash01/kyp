json.extract! constituency, :id, :created_at, :updated_at
json.url constituency_url(constituency, format: :json)

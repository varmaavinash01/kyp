json.extract! politicalparty, :id, :created_at, :updated_at
json.url politicalparty_url(politicalparty, format: :json)
